import React, { Component } from 'react';
import './App.css';

import Transactions from './Transactions'
import Header from './Header'
import Main from './Main'

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      countries: [],
      store: []
    };
  }

  componentDidMount() {
    fetch("https://paymium.com/api/v1/countries")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
            store: result,
            countries: result.map((item) => (
              item.name_fr
            ))
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items, countries } = this.state;
    console.log(countries)
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (      
        <section className="main">
        
          <Transactions items={items} />
        </section>
      );
    }
  }
}

export default Home;
