import React from 'react'
import { Link } from 'react-router-dom'

import './Header.css'

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
  <header>
      <ul>
        <li><Link className="link" to='/'>Overviews</Link></li>
        <li><Link className="link" to='/transactions'>Transactions</Link></li>
        <hr />
        <li><Link className="link" to='/'>Transafers</Link></li>
        <li><Link className="link" to='/'>Invoices</Link></li>
        <hr />
        <li><Link className="link" to='/'>Manage cards</Link></li>
        <li><Link className="link" to='/'>Manage accounts</Link></li>
        <hr />
        <li><Link className="link" to='/'>Team</Link></li>
        <li><Link className="link" to='/'>Integration</Link></li>
        <li><Link className="link" to='/'>Settings</Link></li>
        
      </ul>
  </header>
)

export default Header
