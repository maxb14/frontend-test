import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Header from './Header'
import Main from './Main'

class App extends Component {
  render() {
    return (
      <main>
        <div className="top">
          <div className="intop">
            FINPAL
          </div>
        </div>
        <Header />
        <Main />
      </main>
    );
  }
}

export default App;
