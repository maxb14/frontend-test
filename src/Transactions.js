import React from 'react'
 
const Transactions = (props) =>{
  const{items} = props
  return(
    <table>
    <tr>
      <th>Symbol</th>
      <th>Symbol 2</th>
      <th>Name country</th>
      <th>Telephone code</th>
    </tr>
      {items.map((item) => (
        <tr>  
          <td>{item.iso_alpha2}</td>
          <td>{item.iso_alpha3}</td>
          <td key={item.iso_alpha2}>{item.name_fr}</td>             
          <td>{item.telephone_code}</td>
        </tr>
      ))}

    </table>
  )
}
 
export default Transactions